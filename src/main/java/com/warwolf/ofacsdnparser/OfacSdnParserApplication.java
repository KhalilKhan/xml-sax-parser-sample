package com.warwolf.ofacsdnparser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.tempuri.sdnlist.SdnList;
import org.xml.sax.SAXException;

@SpringBootApplication
public class OfacSdnParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfacSdnParserApplication.class, args);
	}

	@PostConstruct
	public void readXml(){
		
        SAXParserFactory factory = SAXParserFactory.newInstance();

        try (InputStream is = getClass().getClassLoader().getResourceAsStream("sample.xml")) {

            SAXParser saxParser = factory.newSAXParser();

            // parse XML and map to object, it works, but not recommend, try JAXB
            SdnEntrySaxHandler handler = new SdnEntrySaxHandler();

            saxParser.parse(is, handler);

            // print all
            List<SdnList.SdnEntry> result = handler.getSdnEntries();
            result.forEach(sdn -> {
                System.out.println(sdn.getUid());
                System.out.println(sdn.getFirstName());
                System.out.println(sdn.getLastName());
                System.out.println(sdn.getTitle());
                System.out.println(sdn.getSdnType());
                System.out.println(sdn.getRemarks());
            });

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

	}
}
