package com.warwolf.ofacsdnparser;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.tempuri.sdnlist.SdnList;
import org.tempuri.sdnlist.SdnList.SdnEntry;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SdnEntrySaxHandler extends DefaultHandler  {

    private StringBuilder currentValue = new StringBuilder();
    List<SdnList.SdnEntry> sdnEntries;
    SdnList.SdnEntry sdnEntry;

    public List<SdnList.SdnEntry> getSdnEntries() {
        return sdnEntries;
    }

    @Override
    public void startDocument() {
        sdnEntries = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
         // reset the tag value
         currentValue.setLength(0);
         if (qName.equalsIgnoreCase("sdnEntry")) {
            sdnEntry = new SdnEntry();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equalsIgnoreCase("uid") && NumberUtils.isDigits(currentValue.toString())){
            sdnEntry.setUid(NumberUtils.toInt(currentValue.toString()));
        }
        if(qName.equalsIgnoreCase("firstName")){
            sdnEntry.setFirstName(currentValue.toString());
        }
        if(qName.equalsIgnoreCase("lastName")){
            sdnEntry.setLastName(currentValue.toString());
        }
        if(qName.equalsIgnoreCase("title")){
            sdnEntry.setTitle(currentValue.toString());
        }
        if(qName.equalsIgnoreCase("sdnType")){
            sdnEntry.setSdnType(currentValue.toString());
        }
        if(qName.equalsIgnoreCase("remarks")){
            sdnEntry.setRemarks(currentValue.toString());
        }
        if(qName.equalsIgnoreCase("sdnEntry")){
            sdnEntries.add(sdnEntry);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue.append(ch, start, length);
    }
}
